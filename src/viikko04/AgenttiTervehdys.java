package viikko04;

public class AgenttiTervehdys {

    public static void main(String[] args) {
        tervehdi("James", "Bond");
        tervehdi("Gracie", "Hart");
    }

    public static void tervehdi(String etu, String suku) {
        System.out.println("Nimeni on " + suku + ", " + etu + " " + suku);
    }
}
