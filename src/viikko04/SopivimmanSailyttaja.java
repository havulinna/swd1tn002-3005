package viikko04;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class SopivimmanSailyttaja {

    public static void main(String[] args) {
        Scanner lukija = new Scanner(System.in);

        List<String> sanat = Arrays.asList("tomaatti", "selleri", "porkkana");

        System.out.print("Anna etsittävä sana: ");
        String etsittava = lukija.next();

        int loytyiIndeksista = -1;
        for (int i = 0; i < sanat.size(); i++) {
            String nykyinenSana = sanat.get(i);
            if (nykyinenSana.equals(etsittava)) {
                loytyiIndeksista = i;
            }
        }

        if (loytyiIndeksista != -1) {
            System.out.println("Löytyi indeksistä " + loytyiIndeksista);
        } else {
            System.out.println("Ei löytynyt!");
        }

        System.out.println(sanat);
    }
}
