package viikko04;

public class PintaAlaLaskuri {

    public static void main(String[] args) {
        double hyp = 10.7;
        double kork = 4.3;

        double pintaAla = KolmionPintaAla.laskeAla(hyp, kork);

        System.out.println("Kolmion ala on " + pintaAla);
    }

}
