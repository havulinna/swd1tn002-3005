package viikko04;

public class KolmionPintaAla {

    public static double laskeAla(double hypotenuusa, double korkeus) {
        return hypotenuusa * korkeus / 2.0;
    }
}
