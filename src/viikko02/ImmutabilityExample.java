package viikko02;

public class ImmutabilityExample {

    public static void main(String[] args) {
        String text = "Lorem ipsum";

        text.toUpperCase();
        System.out.println(text);

        text = text.toUpperCase();
        System.out.println(text);
    }
}
