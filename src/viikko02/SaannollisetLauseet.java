package viikko02;

public class SaannollisetLauseet {

    public static void main(String[] args) {
        String opiskelijanumero = "a1234567";

        String sallittuMuoto = "a\\d{7}";

        boolean onSallittu = opiskelijanumero.matches(sallittuMuoto);

        if (onSallittu) {
            System.out.println("On oikea opiskelijanumero!");
        } else {
            System.out.println("Ei ole oikea opiskelijanumero!");
        }
    }
}
