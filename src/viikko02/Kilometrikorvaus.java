package viikko02;

import java.util.Scanner;

public class Kilometrikorvaus {

    public static void main(String[] args) {
        Scanner lukija = new Scanner(System.in);
        int summa = 0;

        while (true) {
            System.out.print("Anna ajetut kilometrit (0 lopettaa): ");
            int kilometrit = lukija.nextInt();
            summa = summa + kilometrit;

            if (kilometrit == 0) {
                break;
            }
        }
        System.out.println(summa);
        lukija.close();
    }
}
