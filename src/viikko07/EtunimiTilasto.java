package viikko07;

import java.util.HashMap;
import java.util.List;

public class EtunimiTilasto {

    public static void main(String[] args) {
        TiedostonLukija lukija = new TiedostonLukija("etunimet.csv");
        List<String> rivit = lukija.lueRivit();
        HashMap<String, Integer> tilasto = new HashMap<>();

        for (String rivi : rivit) {
            String[] osat = rivi.split(";");

            String nimi = osat[0];
            String lukumaara = osat[1];
            lukumaara = lukumaara.replaceAll(" ", "");
            int maara = Integer.parseInt(lukumaara);

            if (tilasto.containsKey(nimi)) {
                int edellinenMaara = tilasto.get(nimi);
                tilasto.put(nimi, edellinenMaara + maara);
            } else {
                tilasto.put(nimi, maara);
            }
        }

        System.out.println("Tuiskuja: " + tilasto.getOrDefault("Tuisku", 0));
        System.out.println("Wolverineja: " + tilasto.getOrDefault("Wolverine", 0));
        System.out.println(tilasto);
    }
}
