package viikko01;

public class IntegerOverflow {

    public static void main(String[] args) {
        // Tapahtuu ns. ylivuoto:
        System.out.println(2_147_483_647 + 1);

        // Luku on nyt Long-tyyppinen, jolloin ylivuotoa ei tapahdu:
        System.out.println(2_147_483_647L + 1);
    }
}
