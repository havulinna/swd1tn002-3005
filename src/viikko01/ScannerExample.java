package viikko01;

import java.util.Scanner;

public class ScannerExample {

    public static void main(String[] args) {
        Scanner lukija = new Scanner(System.in);
        System.out.println("Kirjoita kolme lukua: ");

        double eka = lukija.nextDouble();
        double toka = lukija.nextDouble();
        double kolmas = lukija.nextDouble();

        System.out.println("eka: " + eka);
        System.out.println("toka: " + toka);
        System.out.println("kolmas: " + kolmas);

        lukija.close();
    }
}
