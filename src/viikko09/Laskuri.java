package viikko09;

public class Laskuri {

    public int laskeSumma(int[] luvut) {
        int tulos = 0;
        for (int luku : luvut) {
            tulos += luku;
        }
        return tulos;
    }
}
