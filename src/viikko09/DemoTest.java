package viikko09;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class DemoTest {

    @Test
    public void testSumOfIntegers() {
        int result = 4 + 3;

        assertEquals(7, result);
    }

    @Test
    public void testMultiplicationOfIntegers() {
        int result = 4 * 3;

        assertEquals(12, result);
    }
}
