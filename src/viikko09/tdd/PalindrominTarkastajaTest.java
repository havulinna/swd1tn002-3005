package viikko09.tdd;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PalindrominTarkastajaTest {

    private PalindrominTarkastaja tarkastaja;

    @BeforeEach
    public void setUp() {
        this.tarkastaja = new PalindrominTarkastaja();
    }

    @Test
    public void testTyhjaMerkkijonoEiOlePalindromi() {
        boolean onko = tarkastaja.onkoPalindromi("");
        assertFalse(onko);
    }

    @Test
    public void testEiPalindromiPalauttaaFalse() {
        assertFalse(tarkastaja.onkoPalindromi("pallo"));
    }

    @Test
    public void testYksisanainenPalindromiPalauttaaTrue() {
        assertTrue(tarkastaja.onkoPalindromi("saippuakivikauppias"));
    }

    @Test
    public void testKirjainkoollaEiOleMerkitysta() {
        assertTrue(tarkastaja.onkoPalindromi("SaippuaKiviKauppias"));
    }

    @Test
    public void testMonisanainenPalindromi() {
        assertTrue(tarkastaja.onkoPalindromi("innostunut sonni"));
    }

    @Test
    public void testMonimutkainenPalindromiUseillaValimerkeilla() {
        assertTrue(tarkastaja.onkoPalindromi("Atte-kumiorava, varo imuketta!"));
    }

    @Test
    public void testYoEiOlePalindromi() {
        assertFalse(tarkastaja.onkoPalindromi("yö"));
    }
}
