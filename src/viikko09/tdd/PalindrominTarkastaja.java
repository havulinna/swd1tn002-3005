package viikko09.tdd;

public class PalindrominTarkastaja {

    public boolean onkoPalindromi(String teksti) {
        teksti = teksti.toLowerCase().replaceAll("[^a-zåäö]", "");

        StringBuilder builder = new StringBuilder(teksti);
        String kaannetty = builder.reverse().toString();

        return !teksti.isEmpty() && teksti.equals(kaannetty);
    }
}
