package viikko09;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class LaskuriTest {

    private Laskuri laskuri;

    @BeforeEach
    public void setUp() {
        this.laskuri = new Laskuri();
    }

    @Test
    public void testSumOfTwoIntegers() {
        int[] luvut = new int[] { 3, 4 };

        int tulos = laskuri.laskeSumma(luvut);
        assertEquals(7, tulos);
    }

    @Test
    public void testSumFromEmptyArray() {
        int[] luvut = new int[0];

        int tulos = laskuri.laskeSumma(luvut);
        assertEquals(0, tulos);
    }
}
