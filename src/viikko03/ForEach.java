package viikko03;

import java.util.ArrayList;

public class ForEach {

    public static void main(String[] args) {
        ArrayList<String> kuskit = new ArrayList<>();

        kuskit.add("Hamilton");
        kuskit.add("Räikkönen");
        kuskit.add("Bottas");

        for (String kuski : kuskit) {
            System.out.println(kuski.toUpperCase());
        }
    }
}
