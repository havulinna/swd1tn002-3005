package viikko03;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SanojenAlkukirjaimet {

    public static void main(String[] args) {
        Scanner lukija = new Scanner(System.in);
        List<String> sanat = new ArrayList<>();

        for (int i = 1; i <= 5; i++) {
            System.out.print("Syötä sana " + i + "/5: ");
            String sana = lukija.nextLine();
            sanat.add(sana);
        }

        ArrayList<String> alkukirjaimet = new ArrayList<>();

        for (int i = 0; i < sanat.size(); i++) {
            String sana = sanat.get(i);

            String etukirjain = sana.substring(0, 1);
            alkukirjaimet.add(etukirjain);
        }

        System.out.println("Sanat: " + sanat);
        System.out.println("Alkukirjaimet: " + alkukirjaimet);

        lukija.close();
    }
}
