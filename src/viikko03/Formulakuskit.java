package viikko03;

import java.util.ArrayList;

public class Formulakuskit {

    public static void main(String[] args) {
        ArrayList<String> kuskit = new ArrayList<>();

        kuskit.add("Hamilton");
        kuskit.add("Räikkönen");
        kuskit.add("Bottas");

        System.out.println(kuskit);
    }
}
