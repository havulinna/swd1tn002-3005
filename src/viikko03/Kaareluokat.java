package viikko03;

import java.util.ArrayList;

public class Kaareluokat {

    public static void main(String[] args) {
        Integer i = new Integer(1);
        int j = 2;

        System.out.println(i + j);

        ArrayList<Integer> luvut = new ArrayList<>();
        luvut.add(100);
        luvut.add(102);
        luvut.add(50);
        luvut.add(1);

        System.out.println(luvut);

        luvut.remove(new Integer(1));

        System.out.println(luvut);
    }
}
