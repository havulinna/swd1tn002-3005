package viikko06;

import java.util.Arrays;

public class TaulukonLoppuosanKopiointi {

    public static void main(String[] args) {
        String merkkijono = "Mustan kissan paksut pyöreät posket";
        String[] taulukko = merkkijono.split(" ");

        String[] loppuosa = Arrays.copyOfRange(taulukko, taulukko.length - 2, taulukko.length);
        System.out.println(Arrays.toString(loppuosa));
    }
}
