package viikko06;

import java.util.ArrayList;
import java.util.List;

public class TaulukotJaListat {

    public static void main(String[] args) {
        // Luodaan 10 merkkijonon pituinen taulukko:
        String[] taulukko = new String[10];

        // Lisätään sana taulukkoon:
        taulukko[0] = "taulukkoon";

        System.out.println(taulukko.length); // 10

        // Luodaan tyhjä merkkijonolista:
        List<String> lista = new ArrayList<>();

        // Lisätään sana listaan
        lista.add("listalle");

        System.out.println(lista.size()); // 1
    }
}
