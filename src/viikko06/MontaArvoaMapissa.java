package viikko06;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MontaArvoaMapissa {

    public static void main(String[] args) {
        Map<String, List<String>> maat = new HashMap<>();

        List<String> fi = new ArrayList<String>();
        fi.add("Helsinki");
        fi.add("Espoo");
        fi.add("Vantaa");

        List<String> sv = new ArrayList<String>();
        sv.add("Tukholma");
        sv.add("Visby");

        maat.put("Suomi", fi);
        maat.put("Ruotsi", sv);

        System.out.println(fi);
        System.out.println(maat);

        List<String> kaupungit = maat.get("Suomi");
        kaupungit.add("Turku");

        /*
         * Kaupungin lisääminen vaikuttaa sekä siihen muuttujaan, jonka kautta lisäys
         * tehtiin, että kaikkii muihin viittauksiin, jotka viittaavat samaan listaan.
         */
        System.out.println(kaupungit);
        System.out.println(maat);
        System.out.println(fi);
    }
}
