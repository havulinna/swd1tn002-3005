package viikko06;

public class NullIntegerValue {
    public static void main(String[] args) {

        // Luku 1 voidaan esittää alkeistyyppinä tai oliona:
        int a = 1;
        Integer b = 2;

        // Java tekee tyyppimuunnokset automaattisesti:
        int c = b;
        Integer d = a;

        // Olioviittaukset voivat olla 'null', mutta alkeistyypit eivät:
        Integer e = null;
        int f = e;

        System.out.println(a + b + c + d + e + f);
    }
}