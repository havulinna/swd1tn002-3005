package viikko06;

public class PilkoMerkkijono {

    public static void main(String[] args) {
        String teksti = "sana sanat sanoja";

        // pilkotaan välilyöntien kohdalta
        String[] sanat = teksti.split(" ");

        System.out.println(sanat.length);
        System.out.println(sanat[0]);
        System.out.println(sanat[1]);
        System.out.println(sanat[2]);
    }
}
