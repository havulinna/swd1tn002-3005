package viikko06;

import java.util.HashSet;
import java.util.Set;

public class TulostaSetinSisalto {

    public static void main(String[] args) {
        Set<String> words = new HashSet<String>();

        words.add("Foo");
        words.add("Bar");

        words.add("Bar");

        System.out.println(words.size()); // Mikä on nyt joukon koko?

        // Arvojen läpikäynti for-each -silmukalla
        for (String w : words) {
            System.out.println(w);
        }

        // Missä järjestyksessä joukon arvot tulostuvat?
    }
}
