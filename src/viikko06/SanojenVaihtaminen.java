package viikko06;

import java.util.Arrays;

public class SanojenVaihtaminen {

    public static void main(String[] args) {
        String lause = "Parempi pyy pivossa kuin kymmenen oksalla";
        String[] sanat = lause.split(" ");

        int eka = 2;
        int toka = 5;

        String ekaSana = sanat[eka];
        String tokaSana = sanat[toka];

        sanat[eka] = tokaSana;
        sanat[toka] = ekaSana;

        System.out.println(Arrays.toString(sanat));

        String tulos = String.join(" ", sanat);
        System.out.println(tulos);
    }
}
