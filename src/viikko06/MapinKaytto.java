package viikko06;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapinKaytto {

    public static void main(String[] args) {
        Map<String, Integer> opintopisteet = new HashMap<String, Integer>();

        // Lisätään arvoja tietyille avaimille:
        opintopisteet.put("swd1tn001", 5);
        opintopisteet.put("swd1tn002", 5);

        // Haetaan yksi arvo:
        int pisteet = opintopisteet.get("swd1tn002");
        System.out.println(pisteet); // 5

        // Haetaan kaikki avaimet:
        Set<String> avaimet = opintopisteet.keySet();
        System.out.println(avaimet); // [swd1tn002, swd1tn001]
    }
}
