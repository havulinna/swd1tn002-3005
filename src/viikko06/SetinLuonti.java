package viikko06;

import java.util.HashSet;
import java.util.Set;

public class SetinLuonti {

    public static void main(String[] args) {
        Set<String> words = new HashSet<>();

        words.add("Foo");
        words.add("Bar");

        words.add("Bar"); // On jo joukossa, joten mitään ei tapahdu.

        System.out.println(words.size());

        for (String w : words) {
            System.out.println(w);
        }

    }
}
