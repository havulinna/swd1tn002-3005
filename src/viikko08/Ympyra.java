package viikko08;

public class Ympyra extends Muoto {

    private double sade;

    public Ympyra(double sade) {
        super("ympyrä");
        this.sade = sade;
    }

    @Override
    public double pintaAla() {
        return Math.PI * sade * sade;
    }
}
