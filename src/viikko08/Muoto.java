package viikko08;

public abstract class Muoto implements Comparable<Muoto> {
    private String nimi;

    public Muoto(String nimi) {
        this.nimi = nimi;
    }

    public String getNimi() {
        return this.nimi;
    }

    @Override
    public String toString() {
        return this.getNimi() + ": " + this.pintaAla();
    }

    public abstract double pintaAla();

    @Override
    public int compareTo(Muoto toinen) {
        if (this.pintaAla() < toinen.pintaAla()) {
            return -1;
        }
        if (toinen.pintaAla() < this.pintaAla()) {
            return 1;
        }
        return 0;
    }
}
