package viikko08;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MuotojenJarjestaminen {

    public static void main(String[] args) {
        Ympyra y1 = new Ympyra(12.0);
        Ympyra y2 = new Ympyra(6.0);
        Ympyra y3 = new Ympyra(15.0);
        Ympyra y4 = new Ympyra(1.0);
        Ympyra y5 = new Ympyra(31.0);

        List<Muoto> muodot = Arrays.asList(y1, y2, y3, y4, y5);

        Collections.sort(muodot);

        System.out.println(muodot);
    }
}
