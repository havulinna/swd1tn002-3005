package viikko05;

public class OliovittauksenKopiointi {

    public static void main(String[] args) {
        Auto a1 = new Auto("Audi");

        Auto a2 = a1;

        a2.aja(100);

        System.out.println(a1);
        System.out.println(a2);
    }
}
