package viikko05;

public class AutoOhjelma {

    public static void main(String[] args) {

        Auto auto1 = new Auto("Tesla");
        Auto auto2 = new Auto("BMW");

        auto1.aja(100);
        auto2.aja(98);
        auto1.aja(23);

        System.out.println(auto1);
        System.out.println(auto2);
    }
}
