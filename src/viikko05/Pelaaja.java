package viikko05;

public class Pelaaja {

    private int pituus;
    private String nimi;

    public Pelaaja(int pituus, String nimi) {
        this.pituus = pituus;
        this.nimi = nimi;
    }

    public int getPituus() {
        return this.pituus;
    }

    public void setPituus(int pituus) {
        if (pituus < 0 || pituus > 300) {
            throw new IllegalArgumentException();
        }
        this.pituus = pituus;
    }

    public String getNimi() {
        return this.nimi;
    }
}
