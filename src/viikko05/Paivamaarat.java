package viikko05;

import java.time.LocalDate;
import java.time.LocalTime;

public class Paivamaarat {

    public static void main(String[] args) throws InterruptedException {
        LocalDate tanaan = LocalDate.now();

        System.out.println(tanaan);

        int vuosi = tanaan.getYear();
        System.out.println(vuosi);

        int paivaVuonna = tanaan.getDayOfYear();
        System.out.println(paivaVuonna);

        while (true) {
            LocalTime nyt = LocalTime.now();
            System.out.println(nyt);

            Thread.sleep(1000);
        }
    }
}
