package viikko05;

import java.util.Arrays;
import java.util.List;

/**
 * Demo Playlist-tehtävän toString-metodia varten:
 */
public class GathererVariable {

    public static void main(String[] args) {
        List<String> list = Arrays.asList("Hamilton", "Bottas", "Räikkönen");

        String output = listToString(list);
        System.out.println(output);
    }

    private static String listToString(List<String> names) {
        String rows = "";
        for (int i = 0; i < names.size(); i++) {
            String row = (i + 1) + ". " + names.get(i);
            rows = rows + row + "\n";
        }
        return rows.trim();
    }
}
