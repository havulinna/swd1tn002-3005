package viikko05;

import java.util.ArrayList;
import java.util.List;

public class PelaajienPituudet {

    public static void main(String[] args) {

        List<Pelaaja> pelaajat = new ArrayList<Pelaaja>();
        pelaajat.add(new Pelaaja(213, "Lauri"));
        pelaajat.add(new Pelaaja(194, "Petteri"));

        int pituudet = laskeYhteispituus(pelaajat);
        System.out.println("Pituus on yhteensä: " + pituudet);
    }

    public static int laskeYhteispituus(List<Pelaaja> pelaajat) {
        int summa = 0;
        for (int i = 0; i < pelaajat.size(); i++) {
            Pelaaja p = pelaajat.get(i);
            int pituus = p.getPituus();
            summa = summa + pituus;
        }
        return summa;
    }
}
